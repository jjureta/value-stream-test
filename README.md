# How to create Value Stream stats

## Documentation

Please see: https://about.gitlab.com/blog/2016/09/21/cycle-analytics-feature-highlight/

## Configuration

Add different stages in .yml file. To indicate deployment in the production add `environment: production`.

## Using the issue board

1. Create new issue
2. Move issue to "To Do"
3. Click on issue and create a new branche (attention you have to choose to create new branche). 
4. Make changes and commit. In the comment you should add reference to your issue, ex. #2 (where 2 is id of the issue)
4. Create merge request. In the comment you should see "Closes #2" (#2 issue id)
5. Accept the merge request
6. Wait till a pipeline is completed
7. Go to "Cycle Analytics" you should see the stat

## Without the issue board (THIS IS NOT WORKING!)

1. Create a branche
2. Create a merge request
3. Accept the merge request
4. Wait till a pipeline is completed
5. Go to "Cycle Analytics" you should see the stat 


